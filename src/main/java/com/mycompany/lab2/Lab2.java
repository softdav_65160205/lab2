/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static char[][] Table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char Player = 'X';
    static int row, col;

    static void printWelcome() {
        System.out.println("Welcome XO");
    }

    static void printTurnPlayer() {
        System.out.println(Player + " Turn");
    }

    static void inpurRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Please input row, col:");
            row = sc.nextInt();
            col = sc.nextInt();
            if (Table[row - 1][col - 1] != '-') {
                continue;
            }
            Table[row - 1][col - 1] = Player;
            break;
        }
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(Table[i][j] + " ");
            }
            System.out.println();
        }

    }

    static void newTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Table[i][j] = '-';
            }
            System.out.println();
        }

    }

    static void swichPlayer() {
        if (Player == 'X') {
            Player = 'O';
        } else {
            Player = 'X';
        }
    }

    static boolean checkWin() {
        if (checkRow() || checkCol() || checkX()) {
            return true;
        }
        return false;
    }

    static boolean checkDraw() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (Table[r][c] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (Table[row - 1][i] != Player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (Table[j][col - 1] != Player) {
                    return false;
                }
            }
        }
        return true;
    }

    static boolean checkX() {
        if (Table[0][0] == Player && Table[1][1] == Player && Table[2][2] == Player || Table[0][2] == Player && Table[1][1] == Player && Table[2][0] == Player) {
            return true;
        }
        return false;
    }

    static boolean restartGame() {
        Scanner sc = new Scanner(System.in);
        String restart;
        System.out.println("Restart Game (Y/N)??? ");
        restart = sc.next();
        if ("Y".equals(restart)) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            while (true) {
                printTable();
                printTurnPlayer();
                inpurRowCol();
                if (checkWin()) {
                    printTable();
                    System.out.println(Player + " Win!!!");
                    break;
                } else if (checkDraw()) {
                    printTable();
                    System.out.println("Draw");
                    break;
                }
                swichPlayer();
            }
            if (restartGame()) {
                newTable();
                continue;
            } else {
                break;
            }

        }
    }
}
